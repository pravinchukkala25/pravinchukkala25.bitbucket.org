
(function($){

	$(window).scroll(function(){
		var x = $(window).scrollTop();
		$(".animate-progress:hidden").css("display","none");
		if(x>=10){
			$("header").css("background-color","#000");
		}
		else if(x<10){
			$("header").css("background-color","transparent");
		}
	});

	$('.scroll-btn').click(function(){
		var newOffset = $('#what-we-do').offset().top;
		var newheaderHeight = $('header').outerHeight();
		$('html,body').animate({
			'scrollTop': newOffset - newheaderHeight
		}, 800);
	});

})(jQuery);