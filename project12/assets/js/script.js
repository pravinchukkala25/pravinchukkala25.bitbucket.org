(function($){
  var email_reg = /^[\w._~`!@#$%^&\-=\+\\|\[\]'";:.,]+@[\w-_]+\.[a-z.]{2,3}$/;

  // Responsive Menu
  $('.responsive-menu').on('click touch',function() {
    $(this).siblings('ul').slideToggle();
  });

  // Blog social icons toggle
  $('.blog-social').on('click touch', function() {
    $(this).toggleClass('blog-social-icons-hover');
  });

  // Window Scroll
  $(window).scroll(function(){
    if($(window).width() > 768 ) {
      var x = $(window).scrollTop();
      var whyChooseSection = $('.why-choose-us').offset().top;
      var blogSection = $('.offers-blocks').offset().top;
      if(x >= whyChooseSection){
        $('.why-choose-img').addClass('move-img');
      }
      if(x >= blogSection){
        $('.what-we-offer').addClass('move-cat');
      }
    }
  });


  // Window Load
  $(window).load(function(){
    $('.flexslider').flexslider({
      animation: "slide",
      controlNav: false,
      touch: true,
      start: function(slider){
        $('body').removeClass('loading');
      }
    });

    $('.blog-slider').flexslider({
      animation: "slide",
      controlNav: false,
      itemWidth: 210,
      itemMargin: 10,
      maxItems: 3,
      touch: true,
    });
  });

  /*
  * Form Validation Function =====================================
  */
  var validate = function(field, id, regx, range, btn_event) {
    var input_value = $(id).val();
    var reg = regx;

    if(input_value != "" && input_value != null) {
      if(!(reg.test(input_value)) && input_value.length <= range) {
        $(id).siblings('p').removeClass('blank-text');
        $(id).siblings('p').text('Please enter a valid ' + field + '.');
        btn_event.preventDefault(btn_event);      
      } else if( input_value.length > range  ) {
        $(id).siblings('p').removeClass('blank-text');
        $(id).siblings('p').text('User can not physically enter more than ' + range + ' characters.');
        btn_event.preventDefault(btn_event); 
      } else {
        $(id).siblings('p').addClass('blank-text');
      }

    } else {
      $(id).siblings('p').text('Please enter your ' + field + '.');
      btn_event.preventDefault(btn_event);
    }

  }

 /*
  * Form Focus in and Focus out ==================================
  */

  function elements_validate(elemen_id, param1, param2, param_range) {
    $(elemen_id).on('focusout', function() {
      $(elemen_id).siblings('p').removeClass('blank-text');
      validate(param1, elemen_id, param2, param_range, event);
    });

    $(elemen_id).on('focusin', function() {
      $(elemen_id).siblings('p').addClass('blank-text');
    });

     $("#submit-btn").click(function(event) { 
        // Email
        validate( 'email',elemen_id, email_reg, 50, event);
      });

  }

  elements_validate('.form-input', 'email', email_reg, 50);

  $('.form-style').append('<p>&nbsp;</p>');

  // Back to top functionality

  // Scroll (in pixels) after which the "To Top" link is shown
  var offset = 300,
    //Scroll (in pixels) after which the "back to top" link opacity is reduced
    offset_opacity = 1200,
    //Duration of the top scrolling animation (in ms)
    scroll_top_duration = 700,
    //Get the "To Top" link
    $back_to_top = $('#back_to_top');

  //Visible or not "To Top" link
  $(window).scroll(function(){
    ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('top-is-visible') : $back_to_top.removeClass('top-is-visible top-fade-out');
    if( $(this).scrollTop() > offset_opacity ) { 
      $back_to_top.addClass('top-fade-out');
    }
  });

  //Smoothy scroll to top
  $back_to_top.on('click touch', function(event){
    event.preventDefault(event);
    $('body,html').animate({
      scrollTop: 0 ,
    }, scroll_top_duration);
  });

})(jQuery);

