$(function() {

    //PopUp
    $('#welcome-marketplace a').click(function() {
        $('.location-text p').text('You selected ' + $(this).attr('title') + ' location, submit to proceed next step..');
        $('#marketplace').val($(this).data('mktid'));
    });

    //On Click

    $('.location-button').click(function(e) {
        e.preventDefault(true);
        $('.close').trigger('click');
        $('.timeline-map').addClass('animate-map');
        $('.timeline-step2').addClass('animate-step2');
        setTimeout(function() {
            $('.timeline-map').hide();
        }, 1990);

        $('.flex-timeline .step1-menu').addClass('finish');
        $('.flex-timeline .step2-menu').addClass('blue-line');
    });

    $('.pulldata').click(function(){
        $('.timeline-step2').hide();
        $('.timeline-step4').show();
        $('.flex-timeline .step2-menu').addClass('finish');
        $('.flex-timeline .step3-menu').addClass('finish');
        $('.flex-timeline .step4-menu').addClass('blue-line');

    });


});