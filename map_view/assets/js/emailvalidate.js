/**
 * File : addUser.js
 * 
 * This file contain the validation of add user form
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Kishor Mali
 */

$(document).ready(function(){
	
	var addUserForm = $("#emailvalidate");
	
	var validator = addUserForm.validate({
		
		rules:{
			form-email : { required : true, form-email : true}
			
		
		},
		messages:{
			form-email :{ required : "This field is required" }
									 
										 
						
		}
	});
});
