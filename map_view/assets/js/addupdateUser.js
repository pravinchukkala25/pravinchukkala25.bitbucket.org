/**
 * File : addUser.js
 * 
 * This file contain the validation of add user form
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Kishor Mali
 */

$(document).ready(function(){
	
	var addUserForm = $("#addupdateUser");
	
	var validator = addUserForm.validate({
		
		rules:{
			password : { required : true },
			cpassword : {required : true, equalTo: "#password"},
			country :{ required : true },
			phone : { required : true, digits : true }
			
		},
		messages:{
			password : { required : "This field is required" },
			cpassword : {required : "This field is required", equalTo: "Please enter same password" },
			country :{ required : "This field is required" },
			phone : { required : "This field is required", digits : "Please enter numbers only" }
					
		}
	});
});



