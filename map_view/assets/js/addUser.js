/**
 * File : addUser.js
 * 
 * This file contain the validation of add user form
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Kishor Mali
 */

/*$(document).ready(function(){
	
	var addUserForm = $("#addUser");
	
	var validator = addUserForm.validate({
		
		rules:{
			fname :{ required : true },
			email : { required : true, email : true, remote : { url : baseURL + "checkEmailExists", type :"post"} },
			password : { required : true },
			cpassword : {required : true, equalTo: "#password"},
			mobile : { required : true, digits : true },
			role : { required : true, selected : true}
		},
		messages:{
			fname :{ required : "This field is required" },
			email : { required : "This field is required", email : "Please enter valid email address", remote : "Email already taken" },
			password : { required : "This field is required" },
			cpassword : {required : "This field is required", equalTo: "Please enter same password" },
			mobile : { required : "This field is required", digits : "Please enter numbers only" },
			role : { required : "This field is required", selected : "Please select atleast one option" }			
		}
	});
});*/


$(document).ready(function(){
	
	var addUserForm = $("#addUser");
	
	var validator = addUserForm.validate({
		
		rules:{
			form-first-name :{ required : true },
			form-last-name : { required : true },
			form-email : { required : true, form-email : true },
			form-promocode : { required : true },
			form-phone-number : { required : true, digits : true },							 
			form-password : { required : true },
			form-cpassword : {required : true, equalTo: "#form-password"}
		
		},
		messages:{
			form-first-name :{ required : "This field is required" },
			form-last-name : { required : "This field is required" },
			form-email : { required : "This field is required", form-email : "Please enter valid email address" },
			form-promocode : { required : "This field is required" },
			form-phone-number : { required : "This field is required", digits : "Please enter numbers only" },							form-password : { required : "This field is required" },
			form-cpassword : {required : "This field is required", equalTo: "Please enter same password" }							 
										 
						
		}
	});
});
