<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage pcagencytheme
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<!-- FEATURES SECTION -->


<?php if ( is_home() && ! is_front_page() ) : ?>
<header>
	<div id="post-<?php the_ID(); ?>" class="page-title-banner section-container vz-space" style="background-image: url(<?php echo get_theme_file_uri(); ?>/assets/images/title_bg.jpg); background-repeat: no-repeat; background-size: cover;">
		<div class="wrapper">
			<div class="page-block">
				<h2 class="page-title"><?php single_post_title(); ?></h2>
			</div>
		</div>
	</div>
</header>
<?php else : ?>
	<header>
		<div id="post-<?php the_ID(); ?>" class="page-title-banner section-container vz-space" style="background-image: url(<?php echo get_theme_file_uri(); ?>/assets/images/title_bg.jpg);     background-repeat: no-repeat;
    background-size: cover;">
			<div class="wrapper">
				<div class="page-block">
					<h2 class="page-title"><?php echo get_the_title(); ?></h2>
				</div>
			</div>
		</div>
	</header>
<?php endif; ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main vz-space" role="main">
		<div class="wrapper vz-space">
			<?php 
				if(is_page()){

					if( have_posts() ):

						/* Start the Loop */
						while( have_posts() ): the_post();

							the_content();

						endwhile;
					endif;

				} else {
					get_template_part( 'template-parts/post/content');
				}

			 ?>
		</div>

	</main><!-- #main -->
</div><!-- #primary -->


<?php get_footer();
