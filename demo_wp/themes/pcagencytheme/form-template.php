<?php
/**
 * Template Name: Custom Form Template
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage pcagencytheme
 * @since 1.0
 * @version 1.0
 */


get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php while( have_posts() ): the_post(); ?>
			<div class="wrapper">
				<div id="banner-section" class="section-container bg-transparent">
					<div class="aligncenter vz-space">
						<div class="wrapper">
							<div class="form-info">
								<h2 class="banner-title"><?php echo get_the_title(); ?></h2>
								<?php the_content(); ?>
								<a href="#custom_form_post" title="Readmore" class="button button-red" id="form_start">Start</a>
							</div>
							
							<form action="" method="post" id="custom_form_post"	class="home-banner">
								<!-- STEP 1 -->
								<div class="form-info vz-space steps-animate" id="step1">
									<h3 class="form-title text-left text-blue"><span class="text-blue">1.</span> What's your full name?</h3>
									<input  type="text" name="FULLNAME">
								</div>

								<!-- STEP 2 -->
								<div class="form-info vz-space steps-animate" id="step2">
									<h3 class="form-title text-left text-blue"><span class="text-blue">2.</span> If accepted, what email should we send your link to? *</h3>
									<input type="email" name="EMAIL_ID">
								</div>

								<!-- STEP 3 -->
								<div class="form-info vz-space steps-animate" id="step3">
									<h3 class="form-title text-left text-blue"><span class="text-blue">3.</span> Briefly describe your business, who do you serve, what do you sell, what's the price point?</h3>
									<textarea name="ABOUT_BUSINESS" cols="30" rows="1"></textarea>
								</div>

								<!-- STEP 4 -->
								<div class="form-info vz-space steps-animate" id="step4">
									<h3 class="form-title text-left text-blue"><span class="text-blue">4.</span> Your business website (If you don't have one please type "no site") *</h3>
									<input type="text" name="WEBSITE_URL">
								</div>

								<!-- STEP 5 -->
								<div class="form-info vz-space steps-animate" id="step5">
									<h3 class="form-title text-left text-blue"><span class="text-blue">5.</span> Current monthly revenue *</h3>
									<input type="number" name="REVENUE">
								</div>

								<!-- STEP 6 -->
								<div class="form-info vz-space steps-animate" id="step6">
									<h3 class="form-title text-left text-blue"><span class="text-blue">6.</span> What do you feel is your biggest obstacle to hitting your monthly revenue goal?</h3>
									<textarea name="REVENUE_GOAL" cols="30" rows="1"></textarea>
								</div>

								<!-- STEP 7 -->
								<div class="form-info vz-space steps-animate" id="step7">
									<h3 class="form-title text-left text-blue"><span class="text-blue">7.</span> How much you will be able to spend on ads per month?</h3>
									<input type="number" name="ADS_SPEND">
								</div>

								<!-- STEP 8 -->
								<div class="form-info vz-space steps-animate" id="step8">
									<h3 class="form-title text-left text-blue"><span class="text-blue">8.</span> How willing and able are you to invest in the growth of your business right now?</h3>
									<textarea name="GROWTH_INVEST" id="" cols="30" rows="1"></textarea>
								</div>

								<!-- STEP 9 -->
								<div class="form-info vz-space steps-animate" id="step9">
									<h3 class="form-title text-left text-blue"><span class="text-blue">9.</span> If you're accepted how soon can you get started?</h3>
									<input type="text" name="GET_STARTED">
								</div>

								<!-- STEP 10 -->
								<div class="form-info vz-space steps-animate" id="step10">
									<h3 class="form-title text-left text-blue"><span class="text-blue">10.</span> Finally, what makes you different from the other applications and why should we choose to work with you? *</h3>
									<textarea name="FINAL" id="" cols="30" rows="1"></textarea>
								</div>

								<!-- STEP 10 -->
								<div class="form-info vz-space steps-animate">
									<button type="submit" name="csurvery_submit">SCHEDULE</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
	</main><!-- #main -->
</div><!-- #primary -->

<?php wp_footer();