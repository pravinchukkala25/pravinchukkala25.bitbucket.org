<?php
/**
 * pcagencytheme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage pcagencytheme
 * @since 1.0
 */

/**
 * pcagencytheme only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function pcagencytheme_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/pcagencytheme
	 * If you're building a theme based on pcagencytheme, use a find and replace
	 * to change 'pcagencytheme' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'pcagencytheme' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'pcagencytheme-featured-image', 2000, 1200, true );

	add_image_size( 'pcagencytheme-thumbnail-avatar', 100, 100, true );

	// Set the default content width.
	$GLOBALS['content_width'] = 525;

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'top'    => __( 'Top Menu', 'pcagencytheme' ),
		'menu-1' => __('Main Menu','pcagencytheme'),
		'footer' => __('Footer Menu','pcagencytheme'),
		'social' => __( 'Social Links Menu', 'pcagencytheme' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 90,
		'height'      => 50,
		'flex-width'  => false,
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, and column width.
 	 */
	// add_editor_style( array( 'assets/css/editor-style.css', pcagencytheme_fonts_url() ) );

	// Define and register starter content to showcase the theme on new sites.
	$starter_content = array(
		'widgets' => array(
			// Place three core-defined widgets in the sidebar area.
			'sidebar-1' => array(
				'text_business_info',
				'search',
				'text_about',
			),

			// // Add the core-defined business info widget to the footer 1 area.
			// 'sidebar-2' => array(
			// 	'text_business_info',
			// ),

			// // Put two core-defined widgets in the footer 2 area.
			// 'sidebar-3' => array(
			// 	'text_about',
			// 	'search',
			// ),
		),

		// Specify the core-defined pages to create and add custom thumbnails to some of them.
		'posts' => array(
			'home',
			'about' => array(
				'thumbnail' => '{{image-sandwich}}',
			),
			'contact' => array(
				'thumbnail' => '{{image-espresso}}',
			),
			'blog' => array(
				'thumbnail' => '{{image-coffee}}',
			),
			'homepage-section' => array(
				'thumbnail' => '{{image-espresso}}',
			),
		),

		// Create the custom image attachments used as post thumbnails for pages.
		'attachments' => array(
			'image-espresso' => array(
				'post_title' => _x( 'Espresso', 'Theme starter content', 'pcagencytheme' ),
				'file' => 'assets/images/espresso.jpg', // URL relative to the template directory.
			),
			'image-sandwich' => array(
				'post_title' => _x( 'Sandwich', 'Theme starter content', 'pcagencytheme' ),
				'file' => 'assets/images/sandwich.jpg',
			),
			'image-coffee' => array(
				'post_title' => _x( 'Coffee', 'Theme starter content', 'pcagencytheme' ),
				'file' => 'assets/images/coffee.jpg',
			),
		),

		// Default to a static front page and assign the front and posts pages.
		'options' => array(
			'show_on_front' => 'page',
			'page_on_front' => '{{home}}',
			'page_for_posts' => '{{blog}}',
		),

		// Set the front page section theme mods to the IDs of the core-registered pages.
		'theme_mods' => array(
			'panel_1' => '{{homepage-section}}',
			'panel_2' => '{{about}}',
			'panel_3' => '{{blog}}',
			'panel_4' => '{{contact}}',
		),

		// Set up nav menus for each of the two areas registered in the theme.
		'nav_menus' => array(
			// Assign a menu to the "top" location.
			'top' => array(
				'name' => __( 'Top Menu', 'pcagencytheme' ),
				'items' => array(
					'link_home', // Note that the core "home" page is actually a link in case a static front page is not used.
					'page_about',
					'page_blog',
					'page_contact',
				),
			),

			// Assign a menu to the "social" location.
			'social' => array(
				'name' => __( 'Social Links Menu', 'pcagencytheme' ),
				'items' => array(
					'link_yelp',
					'link_facebook',
					'link_twitter',
					'link_instagram',
					'link_email',
				),
			),
		),
	);

	/**
	 * Filters pcagencytheme array of starter content.
	 *
	 * @since pcagencytheme 1.1
	 *
	 * @param array $starter_content Array of starter content.
	 */
	$starter_content = apply_filters( 'pcagencytheme_starter_content', $starter_content );

	add_theme_support( 'starter-content', $starter_content );
}
add_action( 'after_setup_theme', 'pcagencytheme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function pcagencytheme_content_width() {

	$content_width = $GLOBALS['content_width'];

	// Get layout.
	$page_layout = get_theme_mod( 'page_layout' );

	// Check if layout is one column.
	if ( 'one-column' === $page_layout ) {
		if ( pcagencytheme_is_frontpage() ) {
			$content_width = 644;
		} elseif ( is_page() ) {
			$content_width = 740;
		}
	}

	// Check if is single post and there is no sidebar.
	if ( is_single() && ! is_active_sidebar( 'sidebar-1' ) ) {
		$content_width = 740;
	}

	/**
	 * Filter pcagencytheme content width of the theme.
	 *
	 * @since pcagencytheme 1.0
	 *
	 * @param int $content_width Content width in pixels.
	 */
	$GLOBALS['content_width'] = apply_filters( 'pcagencytheme_content_width', $content_width );
}
add_action( 'template_redirect', 'pcagencytheme_content_width', 0 );


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function pcagencytheme_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'pcagencytheme' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'pcagencytheme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 1', 'pcagencytheme' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Add widgets here to appear in your footer.', 'pcagencytheme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 2', 'pcagencytheme' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Add widgets here to appear in your footer.', 'pcagencytheme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'pcagencytheme_widgets_init' );

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since pcagencytheme 1.0
 *
 * @param string $link Link to single post/page.
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function pcagencytheme_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf( '<a href="%1$s" class="button button-red">%2$s</a>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		__( 'Continue reading', 'pcagencytheme' )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'pcagencytheme_excerpt_more' );


/**
 * Filter the excerpt length to 50 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function theme_slug_excerpt_length( $length ) {
	if ( is_admin() ) {
		return $length;
	}
	return 10;
}
add_filter( 'excerpt_length', 'theme_slug_excerpt_length', 999 );

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since pcagencytheme 1.0
 */
function pcagencytheme_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'pcagencytheme_javascript_detection', 0 );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function pcagencytheme_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
	}
}
add_action( 'wp_head', 'pcagencytheme_pingback_header' );

/**
 * Display custom color CSS.
 */
function pcagencytheme_colors_css_wrap() {
	if ( 'custom' !== get_theme_mod( 'colorscheme' ) && ! is_customize_preview() ) {
		return;
	}

	require_once( get_parent_theme_file_path( '/inc/color-patterns.php' ) );
	$hue = absint( get_theme_mod( 'colorscheme_hue', 250 ) );
	?>
	<style type="text/css" id="custom-theme-colors" <?php if ( is_customize_preview() ) { echo 'data-hue="' . $hue . '"'; } ?>>
	<?php echo pcagencytheme_custom_colors_css(); ?>
</style>
<?php }
add_action( 'wp_head', 'pcagencytheme_colors_css_wrap' );

/**
 * Enqueue scripts and styles.
 */
function pcagencytheme_scripts() {

	// Theme stylesheet.
	wp_enqueue_style( 'pcagencytheme-style', get_stylesheet_uri() );

	// Load the dark colorscheme.
	if ( 'dark' === get_theme_mod( 'colorscheme', 'light' ) || is_customize_preview() ) {
		wp_enqueue_style( 'pcagencytheme-colors-dark', get_theme_file_uri( '/assets/css/colors-dark.css' ), array( 'pcagencytheme-style' ), '1.0' );
	}

	// Load the Internet Explorer 9 specific stylesheet, to fix display issues in the Customizer.
	if ( is_customize_preview() ) {
		wp_enqueue_style( 'pcagencytheme-ie9', get_theme_file_uri( '/assets/css/ie9.css' ), array( 'pcagencytheme-style' ), '1.0' );
		wp_style_add_data( 'pcagencytheme-ie9', 'conditional', 'IE 9' );
	}

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'pcagencytheme-ie8', get_theme_file_uri( '/assets/css/ie8.css' ), array( 'pcagencytheme-style' ), '1.0' );
	wp_style_add_data( 'pcagencytheme-ie8', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'pcagencytheme-global', get_theme_file_uri( '/assets/js/global.js' ), array( 'jquery' ), '1.0', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'pcagencytheme_scripts' );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images.
 *
 * @since pcagencytheme 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function pcagencytheme_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 740 <= $width ) {
		$sizes = '(max-width: 706px) 89vw, (max-width: 767px) 82vw, 740px';
	}

	if ( is_active_sidebar( 'sidebar-1' ) || is_archive() || is_search() || is_home() || is_page() ) {
		if ( ! ( is_page() && 'one-column' === get_theme_mod( 'page_options' ) ) && 767 <= $width ) {
			$sizes = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'pcagencytheme_content_image_sizes_attr', 10, 2 );

/**
 * Filter the `sizes` value in the header image markup.
 *
 * @since pcagencytheme 1.0
 *
 * @param string $html   The HTML image tag markup being filtered.
 * @param object $header The custom header object returned by 'get_custom_header()'.
 * @param array  $attr   Array of the attributes for the image tag.
 * @return string The filtered header image HTML.
 */
function pcagencytheme_header_image_tag( $html, $header, $attr ) {
	if ( isset( $attr['sizes'] ) ) {
		$html = str_replace( $attr['sizes'], '100vw', $html );
	}
	return $html;
}
add_filter( 'get_header_image_tag', 'pcagencytheme_header_image_tag', 10, 3 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails.
 *
 * @since pcagencytheme 1.0
 *
 * @param array $attr       Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size       Registered image size or flat array of height and width dimensions.
 * @return array The filtered attributes for the image markup.
 */
function pcagencytheme_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( is_archive() || is_search() || is_home() ) {
		$attr['sizes'] = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
	} else {
		$attr['sizes'] = '100vw';
	}

	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'pcagencytheme_post_thumbnail_sizes_attr', 10, 3 );

/**
 * Use front-page.php when Front page displays is set to a static page.
 *
 * @since pcagencytheme 1.0
 *
 * @param string $template front-page.php.
 *
 * @return string The template to be used: blank if is_home() is true (defaults to index.php), else $template.
 */
function pcagencytheme_front_page_template( $template ) {
	return is_home() ? '' : $template;
}
add_filter( 'frontpage_template',  'pcagencytheme_front_page_template' );

/**
 * Modifies tag cloud widget arguments to display all tags in the same font size
 * and use list format for better accessibility.
 *
 * @since pcagencytheme 1.4
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array The filtered arguments for tag cloud widget.
 */
function pcagencytheme_widget_tag_cloud_args( $args ) {
	$args['largest']  = 1;
	$args['smallest'] = 1;
	$args['unit']     = 'em';
	$args['format']   = 'list';

	return $args;
}
add_filter( 'widget_tag_cloud_args', 'pcagencytheme_widget_tag_cloud_args' );



/**
 * Custom template tags for this theme.
 */
require get_parent_theme_file_path( '/inc/template-tags.php' );

/**
 * SVG icons functions and filters.
 */
require get_parent_theme_file_path( '/inc/icon-functions.php' );


/* SVG Uploads  ======================== */

function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');



/**
 * Generate breadcrumbs
 * @author CodexWorld
 * @authorURL www.codexworld.com
 */
function get_breadcrumb() {
	echo '<a href="'.home_url().'" rel="nofollow">Home</a>';
	if (is_category() || is_single()) {
		echo "&nbsp;&nbsp;-&nbsp;&nbsp;";
		the_category(' &bull; ');
		if (is_single()) {
			echo " &nbsp;&nbsp;-&nbsp;&nbsp; ";
			the_title();
		}
	} elseif (is_page()) {
		echo "&nbsp;&nbsp;-&nbsp;&nbsp;";
		echo the_title();
	} elseif (is_search()) {
		echo "&nbsp;&nbsp;-&nbsp;&nbsp;Search Results for... ";
		echo '"<em>';
		echo the_search_query();
		echo '</em>"';
	}
}




/**
 * Adding custom post function
 *
 * @since pcagencytheme 1.0
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array The filtered arguments for tag cloud widget.
*/

function register_custom_post_type($postSlugName, $postName, $singleName,$slugName)
{
	register_post_type($postSlugName,
		array(
			'labels'      => array(
				'name'          => __($postName),
				'singular_name' => __($singleName),
			),
			'public'      => true,
			'publicly_queryable' => true,
			'has_archive' => false,
			'hierarchical' => true,
			'rewrite'     => array( 'slug' => $slugName ),
			'show_ui'            => true,
			'show_in_menu'       => true,
			'show_in_nav_menus' => true,
			'show_in_rest' => true,
			'supports'           => array( 'title', 'editor', 'comments', 'revisions', 'trackbacks', 'author', 'excerpt', 'page-attributes', 'thumbnail', 'custom-fields')
		)
	);

	add_filter('gutenberg_can_edit_post_type', true, $slugName);
}


function adding_custom_post_type()
{
	register_custom_post_type('vertical_carousel','Vertical Carousel','Vertical Carousel','vertical_carousel');
}
add_action('init', 'adding_custom_post_type');



/**
* =======================================================
* 	Customizer Footer Copyright
* ========================================================
*/
add_action('customize_register','my_customize_register');
function my_customize_register( $wp_customize ) {

	$wp_customize->add_setting( 'footer_copyright', array(
		'type' => 'theme_mod', // or 'option'
		'capability' => 'edit_theme_options',
		'transport' => 'refresh'
	) );

	$wp_customize->add_control( 'footer_copyright', array(
		'type' => 'textarea',
		'section' => 'footer', // Required, core or custom.
		'label' => __( 'Copyright Section' ),
		'description' => __( 'This is a copyright to display on footer.' ),
	) );

		// Add a footer/copyright information section.
	$wp_customize->add_section( 'footer' , array(
		'title' => __( 'Footer Copyright', 'pcagencytheme' ),
		'description' => __( 'Add copyright here' ),
		'panel' => '',
	  	'priority' => 105, // Before Widgets.
	  	'capability' => 'edit_theme_options',
	  	'theme_supports' => '',

	  ) );



	/**************************************
	Front Page Design
	***************************************/	

	$wp_customize->add_panel(
		'panel_frontpage', array(			
			'capability' => 'edit_theme_options',
			'title'      => __( 'Front Page Design', 'pcagencytheme' ),
		)
	);


	/**
	Testimonials Content
	**/	
	$wp_customize->add_section(
		'pcagencytheme_testimonials_section', array(
			'title'    => __( 'Testimonials', 'pcagencytheme' ),
			'priority' => 4,
			'panel'    => 'panel_frontpage',
		)
	);
	
	/**Testimonials Content Inside **/
	$wp_customize->add_setting(
		'enable_testimonials_cont', array(
			'sanitize_callback' => 'pcagencytheme_sanitize_checkbox',
			'transport'         => 'refresh',				
		)
	);

	$wp_customize->add_control(
		'enable_testimonials_cont', array(
			'label'    	  => __( 'Disable Testimonials', 'pcagencytheme' ),
			'description'     => __( 'If Disabled - This will hide Testimonials section from home page', 'pcagencytheme' ),	
			'section' 	  => 'pcagencytheme_testimonials_section',
			'priority' 	  => 1,
			'type'       	  => 'checkbox',

		)
	);	

	$wp_customize->add_setting(
		'testimonials_cont_h', array(
			'sanitize_callback' => 'pcagencytheme_sanitize_clean',
			'transport'         => 'refresh',
			'default'           => $pcagencytheme_options['testimonials_cont_h'],	
		)
	);

	$wp_customize->add_control(
		'testimonials_cont_h', array(
			'label'    		  => __( 'Title', 'pcagencytheme' ),				
			'section' 		  => 'pcagencytheme_testimonials_section',
			'priority' 		  => 2,
			'type'       	  => 'text',				
		)
	);
	$wp_customize->add_setting(
		'testimonials_cont_hs', array(
			'sanitize_callback' => 'pcagencytheme_sanitize_clean',
			'transport'         => 'refresh',
			'default'          => $pcagencytheme_options['testimonials_cont_hs'],
		)
	);

	$wp_customize->add_control(
		'testimonials_cont_hs', array(
			'label'    		  => __( 'Sub Title', 'pcagencytheme' ),				
			'section' 		  => 'pcagencytheme_testimonials_section',
			'priority' 		  => 3,
			'type'                    => 'text',				
		)
	);	
	$wp_customize->add_setting(
		'testimonials_cont_scode', array(
			'sanitize_callback' => 'pcagencytheme_sanitize_clean',
			'transport'         => 'refresh',
			'default'          => $pcagencytheme_options['testimonials_cont_scode'],
		)
	);

	$wp_customize->add_control(
		'testimonials_cont_scode', array(
			'label'          => __( 'Shortcode', 'pcagencytheme' ),
			'description'    => __( 'To check shortcode please go to Testimonials -> How It Work ', 'pcagencytheme' ),	
			'section' 	 => 'pcagencytheme_testimonials_section',
			'priority' 	 => 4,
			'type'       	 => 'textarea',				
		)
	);


}



/* CREATING FRONT END FORM PAGE ================== */
function html_form_code() {
	echo '<div class="popup-container"><div class="wrapper">';
	echo '<form action="'. esc_url($_SERVER['REQUEST']).'" method="post">';
	echo '<p>';
	echo 'Your name <br>';
	echo '<input type="text" name="cf-name" pattern="[a-zA-Z0-9 ]+" value="'. ( isset( $_POST["cf-name"] ) ? esc_attr( $_POST["cf-name"] ) : '' ) . '" size="40" />';
	echo '</p>';
	echo '<p>';
	echo 'Your Email (required) <br />';
	echo '<input type="email" name="cf-email" value="' . ( isset( $_POST["cf-email"] ) ? esc_attr( $_POST["cf-email"] ) : '' ) . '" size="40" />';
	echo '</p>';
	echo '<p>';
	echo 'Subject (required) <br />';
	echo '<input type="text" name="cf-subject" pattern="[a-zA-Z ]+" value="' . ( isset( $_POST["cf-subject"] ) ? esc_attr( $_POST["cf-subject"] ) : '' ) . '" size="40" />';
	echo '</p>';
	echo '<p>';
	echo 'Your Message (required) <br />';
	echo '<textarea rows="10" cols="35" name="cf-message">' . ( isset( $_POST["cf-message"] ) ? esc_attr( $_POST["cf-message"] ) : '' ) . '</textarea>';
	echo '</p>';
	echo '<p><input type="submit" name="cf-submitted" value="Send"/></p>';
	echo '</form>';
	echo '</div></div>';
}




/** 
* SENDING EMAIL TO CLIENT ============================
*
*/

function deliver_mail() {

    // if the submit button is clicked, send the email
	if ( isset( $_POST['cf-submitted'] ) ) {

        // sanitize form values
		$name    = sanitize_text_field( $_POST["cf-name"] );
		$email   = sanitize_email( $_POST["cf-email"] );
		$subject = sanitize_text_field( $_POST["cf-subject"] );
		$message = esc_textarea( $_POST["cf-message"] );

        // get the blog administrator's email address
		$to = get_option( 'admin_email' );

		$headers = "From: $name <$email>" . "\r\n";

        // If email has been process for sending, display a success message
		if ( wp_mail( $to, $subject, $message, $headers ) ) {
			echo '<div>';
			echo '<p>Thanks for contacting me, expect a response soon.</p>';
			echo '</div>';
		} else {
			echo 'An unexpected error occurred';
		}
	}
}



function cf_shortcode() {
	ob_start();
	deliver_mail();
	html_form_code();

	return ob_get_clean();
}
add_shortcode( 'digital_agency_contact_form', 'cf_shortcode' );