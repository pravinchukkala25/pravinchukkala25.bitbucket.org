<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage pcagencytheme
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Font face -->
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<?php if(is_front_page()){ ?>
<!-- 		<link id="load_vertical_crsl_css" rel="stylesheet" href="<?php //echo get_theme_file_uri('assets/vertical_carousel/css/vertical_carousal.min.css') ?>"> -->
	<?php } ?>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	
	<?php if(is_front_page()){ ?>
<!-- 	<script id="load_vertical_crsl_js" async src="<?php //echo get_theme_file_uri('assets/vertical_carousel/js/vertical_carousal.js') ?>"></script> -->
	<?php } ?>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	
	<?php if(is_front_page()){ ?>
	<style>

	.vertical-carousel-indicators {
	    z-index: 1000;
	    position: fixed;
	    top: 50%;
	    right: 30px;
	    text-align: center;
	    padding: 0;
	    margin: -40px 0 0 0;
	}

	.vertical-carousel-indicators li {
	    display: block;
	    position: relative;
	    z-index: 1001;
	    list-style: none;
	    cursor: pointer;
	    width: 20px;
	    height: 20px;
	    padding: 5px;
	}

	.vertical-carousel-indicators li:before {
		content: '';
   		-webkit-transition: all .1s;
	    -moz-transition: all .1s;
	    -ms-transition: all .1s;
	    -o-transition: all .1s;
	    transition: all .1s;
	    display: block;
	    position: absolute;
	    width: 10px;
	    height: 10px;
	    border-radius: 50%;
	    border: 2px solid #000;
	    left: 50%;
	    top: 50%;
	    margin: -2px 0 0 -2px;
	    background: #fff;
	}

	.vertical-carousel-indicators li a {
		position: absolute;
	    right: 0;
	    color: #fddb7e;
	    top: 0;
	    padding-right: 30px;
	    display: block;
	    white-space: nowrap;
	    opacity: 0;
	    -webkit-transition: all .1s;
	    -moz-transition: all .1s;
	    -ms-transition: all .1s;
	    -o-transition: all .1s;
	    transition: all .1s;
	}

	.vertical-carousel-indicators li a:hover {
		opacity: 1;
		color: #fddb7e;
		text-decoration: none;
	}


	.vertical-carousel-indicators li.active a {	opacity: 1;	}

	.vertical-carousel-indicators li.active:before {
	    width: 15px;
	    height: 15px;
	    border-radius: 50%;
	    margin: -5px 0 0 -5px;
	}

	

	#myVerticalCarousel {
        margin: 0;
    }

	.home-video {
		position: fixed;
		background: #000;
		opacity: 0.8;
		width: 100%;
		height: auto;
		left: 0;
		top: 0;
		z-index: -1;
	}

	.home-video video {
		width: auto;
		height: 100%;
	}

	@media screen and (min-width: 972px){
		.vertical-carousel-box .item { min-height: 100vh; }
	}

	</style>

<?php } ?>

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<div id="page" class="site <?php echo $add_gradient; ?>">

		<header id="masthead" class="site-header">
			<!-- <div class="navigation-top"> -->
				<?php 
					// if(!is_page_template(array('getting-started.php', 'form-template.php'))){
						// if ( has_nav_menu( 'top' ) ) { 
						// 	get_template_part( 'template-parts/navigation', 'top' ); 
						// }
					// }
					?>
			<!-- </div> -->
			<div class="primary-menu">
					
					<?php 
					// if(!is_page_template(array('getting-started.php', 'form-template.php'))){
						if ( has_nav_menu( 'menu-1' ) ) { 
							get_template_part( 'template-parts/main', 'menu' ); 
						}
					// }
					?>
			</div>
		</header><!-- #masthead -->


		<div class="site-content-contain">
			<div id="content" class="site-content">

