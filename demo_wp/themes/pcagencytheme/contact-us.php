<?php
/**
 * Template Name: Contact Us
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage pcagencytheme
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php

				while ( have_posts() ) : the_post();
			?>
				
			<!-- FEATURES SECTION -->
			<div id="post-<?php the_ID(); ?>" class="page-banner vz-space clearfix" style="background-image: url(<?php echo get_theme_file_uri(); ?>/assets/images/title_bg.jpg); background-size: cover; background-color: rgba(0,0,0,0.7);">
				<div class="container-fluid">
					<div class="wrapper vz-space">
						<div class="row">
							<div class="col-sm-6">
								<h2 class="page-title"><?php echo get_the_title(); ?></h2>
							</div>
							<div class="col-sm-6">
								<h3 class="text-right page-description"><?php echo get_breadcrumb(); ?></h3>
							</div>
						</div>
					</div>
				</div>
			</div>


			<?php 
			/* 
			* CONTACT US SECTION - DEFINE CUSTOM FIELDS 
			* ***********************************************************
			*/
			
			/* Telephone Section Variables */
			$telephone_icon1 = get_field('telephones_left_icon');
			$telephone_icon2 = get_field('telephones_right_icon');

			$tel_title1 = get_field('telephone_left_title');
			$tel_title2 = get_field('telephone_right_title');

			$tel_text1 = get_field('telephone_left_text');
			$tel_text2 = get_field('telephone_right_text');

			$tel_number1 = get_field('telephone_left_number');
			$tel_number2 = get_field('telephone_right_number');			

			/* Contact Section Variables */
			$sub_title1 = get_field('contact_left_sub_title');
			$sub_title2 = get_field('contact_form_subtitle');

			$first_half1 = get_field('contact_left_first_half_of_title');
			$first_half2 = get_field('contact_form_first_half_title');

			$second_half1 = get_field('contact_left_second_half_of_title');
			$second_half2 = get_field('contact_form_second_half_title');

			$left_desc = get_field('contact_left_description');
			$icon1 = get_field('contact_left_address_icon');
			$icon2 = get_field('contact_left_email_icon');
			$icon3 = get_field('contact_left_phone_icon');

			$addr = get_field('contact_left_address');
			$left_email = get_field('contact_left_email');
			$phone = get_field('contact_left_phone');

			$form_shortcode = get_field('contact_form_shortcode');


			// echo "<pre>";
			// print_r($telephone_icon1);
			// die;
			


			 ?>
			<div class="contact_us">
				<div class="container-fluid bg-white">
					<div class="row">
						<div class="col-sm-6">
							<div class="wrapper hz-space vz-space">
								<div class="row">
									<div class="col-sm-2">
										<img class="img-responsive" src="<?php echo $telephone_icon1['url']; ?>" alt="<?php echo $telephone_icon1['alt']; ?>">
									</div>
									<div class="col-sm-10">
										<div class="text-muted">
											<strong>
												<?php echo $tel_title1; ?>		
											</strong>
										</div>
										<div class="telephone-details">
											<?php echo $tel_text1; ?>
											<strong><?php echo $tel_number1; ?></strong>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
								<div class="wrapper hz-space vz-space">
									<div class="row">
										<div class="col-sm-2">
											<img class="img-responsive" src="<?php echo $telephone_icon1['url']; ?>" alt="<?php echo $telephone_icon1['alt']; ?>">
										</div>
										<div class="col-sm-10">
											<div class="text-muted">
												<strong><?php echo $tel_title2; ?></strong></div>
											<div class="telephone-details">
												<?php echo $tel_text2; ?>
												<strong><?php echo $tel_number2; ?></strong>
											</div>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>

				<div class="container-fluid">
					<div class="row dp-flex">
						<div class="col-sm-6 bg-grey">
							<div class="wrapper	hz-space vz-space">
								<div class="contact-heading">
									<small><?php echo $sub_title1; ?></small>
									<h2 class="golden-white">
										<span class="border"><?php echo $first_half1;  ?></span><?php echo $second_half1;  ?>
									</h2>
								</div>
								<div class="contact-description vz-space">
									<p><?php echo $left_desc; ?></p>
								</div>
								<div class="contact-details">
									<div class="row">
										<div class="col-md-2">
											<img class="img-responsive" src="<?php echo $icon1['url']; ?>" alt="<?php echo $icon1['alt']; ?>">		
										</div>
										<div class="col-md-10"><?php echo $addr; ?></div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<img class="img-responsive" src="<?php echo $icon2['url']; ?>" alt="<?php echo $icon2['alt']; ?>">
										</div>
										<div class="col-md-10"><?php echo $left_email; ?></div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<img class="img-responsive" src="<?php echo $icon3['url']; ?>" alt="<?php echo $icon3['alt']; ?>">
										</div>
										<div class="col-md-10"><?php echo $phone; ?></div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 bg-cream">
							<div class="wrapper	hz-space vz-space">
								<div class="contact-heading">
									<small><?php echo $sub_title2; ?></small>
									<h2 class="golden-black"><span class="border"><?php echo $first_half2; ?></span><?php echo $second_half2; ?></h2>
								</div>
								<div class="contact-description vz-space">
									<?php echo do_shortcode( $form_shortcode ); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<?php
				endwhile; // End of the loop.

			?>

</main><!-- #main -->
</div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer();
