<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage pcagencytheme
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrapper">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			
			<div class="section-container clearfix vz-space">
				<div class="section-row">
					<div class="column column-onethird">
						<?php
						/* Start the Loop */
						while ( have_posts() ) : the_post();

							get_template_part( 'template-parts/post/content', get_post_format() );
							?>
							<?php
								// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) {
								comments_template();
							}


							/* NEXT PREVIOUS POSTS ======================== */
							$prev_post = get_previous_post(); 
							$id = $prev_post->ID ;
							$plink = get_permalink( $id );
							$ptitle = get_the_title($id);

							$next_post = get_next_post();
							$nid = $next_post->ID ;
							$nlink = get_permalink($nid);
							$ntitle = get_the_title($nid);
							?>
							<nav class="navigation post-navigation" role="navigation">
								<h4 class="entry-title">Post navigation</h4>
								<div class="nav-links">
									<a href="<?php echo $plink; ?>" class="button button-border" rel="prev">Previous Post</a>
									<a href="<?php echo $nlink; ?>" class="button button-border border-square"  rel="next">Next Post</a>
								</div>
							</nav>
							
							<?php
						endwhile; // End of the loop.
						?>
					</div>
					<div class="column column-three hz-space sidebar">
						<?php get_sidebar(); ?>
					</div>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->

<?php get_footer();
