<?php
/**
 * Template Name: Getting Started
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 */


get_header(); ?>

<div id="getting-started-page" class="">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php // Show the selected frontpage content.
		while ( have_posts() ) : the_post();
			?>

			<div class="wrapper">
				<div class="section-container bg-transparent">
					<div>
						<h2 class="banner-title aligncenter"><?php echo get_the_title(); ?></h2>
						<?php echo the_content(); ?>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
	</main>
</div>
</div>
<?php wp_footer();