<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage pcagencytheme
 * @since 1.0
 * @version 1.2
 */

?>

</div>
	<?php get_template_part( 'template-parts/footer/footer', 'widgets' ); ?>
</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>