<?php
/**
 * Displays footer widgets if assigned
 *
 * @package WordPress
 * @subpackage pcagencytheme
 * @since 1.0
 * @version 1.0
 */

?>

<footer id="colophon" class="site-footer" role="contentinfo">
	<aside class="widget-area" role="complementary" aria-label="<?php esc_attr_e( 'Footer', 'pcagencytheme' ); ?>">
		<?php
		
		if ( is_active_sidebar( 'sidebar-2' ) ) { ?>
			<div class="widget-column footer-widget-1">
				<?php dynamic_sidebar( 'sidebar-2' ); ?>
			</div>
		<?php }
		if ( is_active_sidebar( 'sidebar-3' ) ) { ?>
			<div class="widget-column footer-widget-2">
				<?php dynamic_sidebar( 'sidebar-3' ); ?>
			</div>
		<?php } 

			if ( has_nav_menu( 'social' ) ) : ?>
			<nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'pcagencytheme' ); ?>">
				<?php
				wp_nav_menu( array(
					'theme_location' => 'social',
					'menu_class'     => 'social-links-menu',
					'depth'          => 1,
					'link_before'    => '<span class="screen-reader-text">',
					'link_after'     => '</span>' . pcagencytheme_get_svg( array( 'icon' => 'chain' ) ),
				) );
				?>
			</nav><!-- .social-navigation -->
			
		<?php endif; 

			if ( has_nav_menu( 'footer' ) ) : ?>
			<nav class="footer-menu" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'pcagencytheme' ); ?>">
				<?php
				wp_nav_menu( array(
					'theme_location' => 'footer',
					 )
				);
				?>
			</nav><!-- .social-navigation -->
			
		<?php endif; ?>

	</aside><!-- .widget-area -->

		<div id="copyright-section">
			<div class="wrapper">
				<p><?php echo get_theme_mod('footer_copyright',''); ?></p>
			</div>
		</div>
</footer><!-- #colophon -->