<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage pcagencytheme
 * @since 1.0
 * @version 1.2
 */

?>

<nav id="top-menu" class="top-navigation navbar navbar-inverse">
	<div class="wrapper">
		<?php wp_nav_menu( array(
			'theme_location' => 'top',
			'menu_class' => 'nav navbar-nav',
		) ); ?>
	</div>
</nav><!-- #site-navigation -->
