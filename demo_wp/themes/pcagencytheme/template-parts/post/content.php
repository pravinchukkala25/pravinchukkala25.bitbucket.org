<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage pcagencytheme
 * @since 1.0
 * @version 1.2
 */

?>


<div class="row">
	<div class="col-md-9">
		<?php
		if ( have_posts() ) :

			/* Start the Loop */
			while ( have_posts() ) : the_post();

			/*
			 * Include the Post-Format-specific template for the content.
			 * If you want to override this in a child theme, then include a file
			 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
			 */

			?>

			<article id="post-<?php the_ID(); ?>">
				<?php 
				$bgColor = "";
				if(is_single()):
					$bgColor = '';  
					?>
					<div class="post-thumbnail">
						<?php 
						$thumbnail_id = get_post_thumbnail_id( get_the_ID() );
						$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true); 
						?>
						<a href="<?php the_permalink(); ?>">
							<img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'pcagencytheme-featured-image'); ?>"  
							srcset="<?php echo get_the_post_thumbnail_url(get_the_ID(),'pcagencytheme-featured-image'); ?> 768w, <?php echo get_the_post_thumbnail_url(get_the_ID(),'thumbnail'); ?> 320w" 
							class="img-responsive"
							alt="<?php echo $alt; ?>">
						</a>
					</div><!-- .post-thumbnail -->
					<?php
				else:
					$bgColor = 'bg-lightyellow border-grey';

				endif;
				?>
				<div class="<?php echo $bgColor; ?>">
					<?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
					<div class="post-thumbnail">
						<?php 
						$thumbnail_id = get_post_thumbnail_id( get_the_ID() );
						$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true); 
						?>
						<a href="<?php the_permalink(); ?>">
							<img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'pcagencytheme-featured-image'); ?>"  
							srcset="<?php echo get_the_post_thumbnail_url(get_the_ID(),'pcagencytheme-featured-image'); ?> 768w, <?php echo get_the_post_thumbnail_url(get_the_ID(),'thumbnail'); ?> 320w" 
							class="img-responsive" 
							alt="<?php echo $alt; ?>">
						</a>
					</div><!-- .post-thumbnail -->
					<?php endif; ?>

					<header class="entry-header">
						<?php

						if ( is_single() ) {
							the_title( '<h3 class="page-title text-capital">', '</h3>' );
						} elseif ( is_front_page() && is_home() ) {
							the_title( '<h3 class="page-title text-capital"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
						} else {
							the_title( '<h3 class="page-title text-capital asdff"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );

							echo '<h5>Author: <span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . get_the_author() . '</a></span></h5>';
							?>

							<div class="entry-description">
								<?php the_excerpt(); ?>
								<?php
								/* translators: %s: Name of current post */

								wp_link_pages( array(
									'before'      => '<div class="page-links">' . __( 'Pages:', 'pcagencytheme' ),
									'after'       => '</div>',
									'link_before' => '<span class="page-number">',
									'link_after'  => '</span>',
								) );
								?>
							</div><!-- .entry-content -->

							<?php
						}

						?>



						<?php
						if ( is_single() ) {
							echo '<h5>Author: <span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . get_the_author() . '</a></span></h5>';
							?>
							<div class="entry-description">
								<?php the_content(); ?>
								<?php
								/* translators: %s: Name of current post */

								wp_link_pages( array(
									'before'      => '<div class="page-links">' . __( 'Pages:', 'pcagencytheme' ),
									'after'       => '</div>',
									'link_before' => '<span class="page-number">',
									'link_after'  => '</span>',
								) );
								?>
							</div><!-- .entry-content -->
							<?php
						}
						?>
					</header><!-- .entry-header -->
				</div>
			</article><!-- #post-## -->


			<?php

			endwhile;
			
			the_posts_pagination( array(
				'prev_text' => '<span class="screen-reader-text button button-red">' . __( 'Previous ', 'pcagencytheme' ) . '</span>',
				'next_text' => '<span class="screen-reader-text button button-red">' . __( 'Next ', 'pcagencytheme' ) . '</span>'
			) );

		endif;
		?>
	</div>
	<div class="col-md-3 hz-space sidebar">
		<?php get_sidebar(); ?>
	</div>
</div>



