<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage pcagencytheme
 * @since 1.0
 * @version 1.0
 */

?>
<!-- FEATURES SECTION -->
<div id="post-<?php the_ID(); ?>" class="section-container page-banner vz-space clearfix" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'full') ?>); background-size: cover; background-color: rgba(0,0,0,0.7);">
	<div class="wrapper clearfix">
		<div class="page-block">
			<div class="page-description"><?php echo get_breadcrumb(); ?></div>
			<h2 class="page-title"><?php echo get_the_title(); ?></h2>
		</div>
	</div>
</div>

<div class="section-container page-content vz-space bg-lightblue">
	<div class="wrapper">
		<?php the_content(); ?>
	</div>
</div>

</div>
