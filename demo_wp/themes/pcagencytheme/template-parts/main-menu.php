<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage pcagencytheme
 * @since 1.0
 * @version 1.2
 */

?>
	<nav id="site-navigation" class="navbar navbar-transparent main-navigation">
		<div class="wrapper">
			<div class="navbar-header">
				<h1 class="custom-logo">
			      	<?php 
			      		the_custom_logo(); 
			      		echo '<span>' . get_bloginfo( 'name' ) . '</span>'; 
			        ?>
			      	
			      </h1>
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span> 
			    </button>
		   
		    </div>
		    <div class="main-menu-container collapse navbar-collapse pull-right" id="myNavbar" aria-expanded="true">
				<?php wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'main-menu',
					'menu_class' => 'nav navbar-nav pull-right',
					'container' => ''
				) );
				 ?>
			</div>
		 </div>
	</nav><!-- #site-navigation -->
