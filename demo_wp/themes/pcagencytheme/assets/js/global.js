/* global pcagencythemeScreenReaderText */
/**
 * Theme functions file.
 *
 * Contains handlers for navigation and widget area.
 */

 (function( $ ) {

 	$('.site-content-contain').css('padding-top',$('.site-header').innerHeight());
 	$('#banner-section').css('height',$(window).height()-$('.site-header').innerHeight() + 'px');

//  	 console.log($('#home_video'));
 	/* Custom Form Script ============== */
	 $(window).on('scroll', function(){
		var y = $(document).scrollTop();
		if(y > 10){
			$('.site-header').css('background-color', '#313131');
		} else {
			$('.site-header').css('background-color', 'transparent');
		}
		 
		 
		if($(window).width() > 768) {
			if($('#total_sections').length){
				var bodyH = $([document.documentElement, document.body]).scrollTop();
				$('.vertical-carousel-box .item').each(function(index, value){
					var elementS = $(".vertical-carousel-indicators a[href='#"+ $(this).attr('id') +"']");
					if(bodyH > ($(this).offset().top) - $('.site-header').innerHeight()) {
						$(this).addClass('active'); 
						elementS.parent('li').addClass('active');
						elementS.parent('li').siblings().removeClass('active');
						$(this).siblings().removeClass('active'); 					
					}
				});
			}

		}


	});

 		if($('#total_sections').length){
	 		
	 		if($(window).width() > 768) {

		 		$('.vertical-carousel-indicators').children('li:last').on('click', function(){
		 			//$('.site-footer').slideToggle();
		 		});

		 		$(".vertical-carousel-indicators a, #main-menu li a").click(function() {
		 			$(this).parent('li').siblings().removeClass('active');
		 			$(this).parent('li').addClass('active');
		 			var targetE = $(this).attr('href');
				    $([document.documentElement, document.body]).animate({
				        scrollTop: $(targetE).offset().top
				    }, 600);
				});
		 	}


		 	if($(window).width() < 769) {
				
				if($('#myVerticalCarousel').length){
			 		//$('#myVerticalCarousel').removeClass('vertical-carousel');
			 	} 	

// 		 		$('#main-menu > .menu-item-has-children').children('a').append('<span class="glyphicon glyphicon-chevron-right pull-right"></span>');

// 			 	$('#main-menu > .menu-item-has-children').on('click', function(){
// 			 		$(this).find('.sub-menu').slideToggle('fast');
// 			 	});

			} else {
// 				if($('#myVerticalCarousel').length){
// 		 			$('#myVerticalCarousel').css('height', $(window).height());
// 		 		}
			}

 		}
	 
	 	$('#main-menu > .menu-item').hover(function(){
	 		var leftP = $(this).offset().left;
	 		if($(this).hasClass('menu-item-has-children')){
	 			$(this).children('ul').css('left',-leftP)
	 		}
	 	});

 })( jQuery );
