<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage pcagencytheme
 * @since 1.0
 * @version 1.0
 */


get_header(); ?>

<!-- <video role="presentation" preload="auto" playsinline="" crossorigin="anonymous" style="opacity: 1; position: absolute; min-width: 1349px; min-height: 759px; left: 0px; top: -185px;" muted="" id="SITE_BACKGROUNDcurrentVideovideo" class="bgVideovideo" width="1349" height="759" src=""></video> -->

<?php 
$get_videoMP4 = get_field('add_video_mp4');
$get_videoWebM = get_field('add_video_webm');
$get_videOgg = get_field('add_video_ogg');
$get_Gif = get_field('add_video_gif');

?>
<div class="home-video">
	<video id="home_video" autoplay muted>
		<?php if($get_videoMP4){ ?>
	  		<source src="<?php echo $get_videoMP4['url']; ?>" type="video/mp4">
	    <?php } ?>
	    <?php if($get_videoWebM){ ?>
	  		<source src="<?php echo $get_videoWebM['url']; ?>" type="video/webm">
	    <?php } ?>
	    <?php if($get_videOgg){ ?>
	  		<source src="<?php echo $get_videOgg['url']; ?>" type="video/ogg">
	    <?php } ?>
	  Your browser does not support the video tag.
	</video>
</div>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<div id="myVerticalCarousel" class="vertical-carousel" data-urlLock="false" data-rests-indicators="#navbar-collapse li">
<?php 

/* Vertical Carousel Post Type ============================= */
$args = array(
	'post_type' => 'vertical_carousel', 
	'posts_per_page'=> -1,
	'order' => 'ASC'
);

$query1 = new WP_Query( $args );

if ( $query1->have_posts() ) {
	?>

	<ol class="vertical-carousel-indicators hidden-xs">
		<?php
		$i = 0;
			// The Loop
			while ( $query1->have_posts() ) {
				$query1->the_post();
				/*if($i===0){
					echo '<li data-target="#myVerticalCarousel" data-slide-to="0" class="active"></li>';
				} else {

					echo '<li data-target="#myVerticalCarousel" data-slide-to="'. $i .'" ></li>';
				}*/
				if($i===0){
					echo '<li class="active"><a href="#'. get_post_field( "post_name", get_the_ID() ) . '">' . get_the_title() . '</a></li>';
				} else {

					echo '<li><a href="#'. get_post_field( "post_name", get_the_ID() ) . '">' . get_the_title() . '</a></li>';
				}

				$i++;
			}
			
			wp_reset_postdata();
		?>
		<li><a href="#contact_section">Contact Us</a></li>
	</ol>
	<?php
}


// CONTAINER ====================================================
?>

<div class="vertical-carousel-box">

<?php
$query2 = new WP_Query( $args );

if ( $query2->have_posts() ) {
	?>

		<?php	$j = 0;		
			// The Loop
			while ( $query2->have_posts() ) {
				$query2->the_post();
				if($j===0){
					echo ' <div id="'. get_post_field( "post_name", get_the_ID() ) . '" class="item active">';
						the_content();
					echo '</div>';
				} else {

					echo ' <div id="'. get_post_field( "post_name", get_the_ID() ) . '" class="item"><div class="container">';
						the_content();
					echo '</div></div>';
				}

				$j++;
			}

			$count_pages = wp_count_posts( $post_type = 'vertical_carousel' );
			if($count_pages){
				?>
				<input type="hidden" id="total_sections" value="<?php echo $j; ?>">
				<?php	
			}
			
			wp_reset_postdata();
		?>
	<?php
}



?>
	<?php 
			/* 
			* CONTACT US SECTION - DEFINE CUSTOM FIELDS 
			* ***********************************************************
			*/
			
			/* Telephone Section Variables */
			$telephone_icon1 = get_field('telephones_left_icon');
			$telephone_icon2 = get_field('telephones_right_icon');

			$tel_title1 = get_field('telephone_left_title');
			$tel_title2 = get_field('telephone_right_title');

			$tel_text1 = get_field('telephone_left_text');
			$tel_text2 = get_field('telephone_right_text');

			$tel_number1 = get_field('telephone_left_number');
			$tel_number2 = get_field('telephone_right_number');			

			/* Contact Section Variables */
			$sub_title1 = get_field('contact_left_sub_title');
			$sub_title2 = get_field('contact_form_subtitle');

			$first_half1 = get_field('contact_left_first_half_of_title');
			$first_half2 = get_field('contact_form_first_half_title');

			$second_half1 = get_field('contact_left_second_half_of_title');
			$second_half2 = get_field('contact_form_second_half_title');

			$left_desc = get_field('contact_left_description');
			$icon1 = get_field('contact_left_address_icon');
			$icon2 = get_field('contact_left_email_icon');
			$icon3 = get_field('contact_left_phone_icon');

			$addr = get_field('contact_left_address');
			$left_email = get_field('contact_left_email');
			$phone = get_field('contact_left_phone');

			$form_shortcode = get_field('contact_form_shortcode');


			 ?>
			<div id="contact_section" class="item contact_us bg-grey">
				<div class="container-fluid">
					<div class="row">
						<div class="col-sm-6">
							<div class="wrapper hz-space vz-space">
								<div class="row">
									<div class="col-sm-2">
										<img class="img-responsive" src="<?php echo $telephone_icon1['url']; ?>" alt="<?php echo $telephone_icon1['alt']; ?>">
									</div>
									<div class="col-sm-10">
										<div>
											<strong>
												<?php echo $tel_title1; ?>		
											</strong>
										</div>
										<div class="telephone-details">
											<?php echo $tel_text1; ?>
											<strong><?php echo $tel_number1; ?></strong>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
								<div class="wrapper hz-space vz-space">
									<div class="row">
										<div class="col-sm-2">
											<img class="img-responsive" src="<?php echo $telephone_icon1['url']; ?>" alt="<?php echo $telephone_icon1['alt']; ?>">
										</div>
										<div class="col-sm-10">
											<div>
												<strong><?php echo $tel_title2; ?></strong></div>
											<div class="telephone-details">
												<?php echo $tel_text2; ?>
												<strong><?php echo $tel_number2; ?></strong>
											</div>
										</div>
									</div>
								</div>
						</div>
					</div>
				</div>

				<div class="container-fluid">
					<div class="row dp-flex">
						<div class="col-sm-6 bg-grey">
							<div class="wrapper	hz-space vz-space">
								<div class="contact-heading">
									<small><?php echo $sub_title1; ?></small>
									<h2 class="golden-white">
										<span class="border"><?php echo $first_half1;  ?></span><?php echo $second_half1;  ?>
									</h2>
								</div>
								<div class="contact-description vz-space">
									<p><?php echo $left_desc; ?></p>
								</div>
								<div class="contact-details">
									<div class="row">
										<div class="col-md-2">
											<img class="img-responsive" src="<?php echo $icon1['url']; ?>" alt="<?php echo $icon1['alt']; ?>">		
										</div>
										<div class="col-md-10"><?php echo $addr; ?></div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<img class="img-responsive" src="<?php echo $icon2['url']; ?>" alt="<?php echo $icon2['alt']; ?>">
										</div>
										<div class="col-md-10"><?php echo $left_email; ?></div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<img class="img-responsive" src="<?php echo $icon3['url']; ?>" alt="<?php echo $icon3['alt']; ?>">
										</div>
										<div class="col-md-10"><?php echo $phone; ?></div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 bg-grey">
							<div class="wrapper	hz-space vz-space">
								<div class="contact-heading">
									<small><?php echo $sub_title2; ?></small>
									<h2 class="golden-white"><span class="border"><?php echo $first_half2; ?></span><?php echo $second_half2; ?></h2>
								</div>
								<div class="contact-description vz-space">
									<?php echo do_shortcode( $form_shortcode ); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php get_template_part( 'template-parts/footer/footer', 'widgets' ); ?>

			</div>

    </div>
</main>


<?php wp_footer(); ?>
</body>
</html>
