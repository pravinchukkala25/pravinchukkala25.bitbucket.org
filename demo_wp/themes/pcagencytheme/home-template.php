<?php
/**
 * Template Name: Home page
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage pcagencytheme
 * @since 1.0
 * @version 1.0
 */


get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php // Show the selected frontpage content.
		if ( have_posts() ) :
			/* Banner Fields */
			$banner_fields = get_field('home_page_banner_section');
			?>

			<div id="banner-section" class="section-container bg-lightgreen">
				<div class="wrapper clearfix">
					<div class="section-row">
						<div class="column column-two hz-space vz-space">
							<?php if($banner_fields): 
								$banner_img = $banner_fields['banner_left_image'];
								$banner_img_sizes = $banner_fields['banner_left_image']['sizes'];
								?>
								<img src="<?php echo $banner_img_sizes['medium']; ?>"  
								srcset="<?php echo $banner_img_sizes['medium']; ?> 640w, <?php echo $banner_img_sizes['thumbnail']; ?> 320w" 
								width="<?php echo $banner_img_sizes['medium-width']; ?>" 
								height="<?php echo $banner_img_sizes['medium-height']; ?>"
								alt="<?php echo $banner_img['alt']; ?>">
							<?php endif; ?>
						</div>
						<div class="column column-two hz-space vz-space">
							<div>
								<h2 class="banner-title"><?php 
								echo $banner_fields['banner_right_title']; 
								?></h2>
								<p class="banner-descrption"><?php echo $banner_fields['banner_right_description']; ?> </p>
								<a href="<?php echo $banner_fields['button_link']; ?>" title="Readmore" class="button button-red">Read more</a>
							</div>
						</div>
					</div>
				</div>
			</div>


			<?php 
			/* Features Fields */
			$services_section_title = get_field('services_section_title');
			$service_desc = get_field('services_section_description');

			$features = get_field('services');

			
			if($services_section_title):
				?>

				<!-- FEATURES SECTION -->
				<div id="features-section" class="section-container vz-space clearfix">
					<div class="upper-layer">
						<div class="wrapper clearfix">
							<div class="main-title positioned-icons">
								<h3 class="entry-title">
									<?php echo $services_section_title; ?>
								</h3>
								<p><?php echo $service_desc; ?></p>
							</div>
							<div class="section-row vz-space">
								<?php 

								for($i=1;$i<=8; $i++){
									$feature_img = $features['feature_icon_'.$i];
									$feature_img_sizes = $features['feature_icon_'.$i]['sizes'];
									if($features['feature_title_'.$i]):
										?>
										<div class="column column-three hz-space vz-space">
											<div class="features-block vz-space">
												<div class="entry-block">
													<div class="icon-block section-row">
														<?php if($feature_img): ?>
															<img src="<?php echo $feature_img_sizes['thumbnail']; ?>" alt="<?php echo $feature_img['alt']; ?>"
															width="90" height="90">
														<?php endif; ?>
														<h3 class="block-title"><?php echo $features['feature_title_'.$i]; ?></h3>
													</div>
													<div class="clearfix"></div>
													<p class="block-description"><?php echo $features['feature_description_' .$i]; ?></p>
													<a href="<?php echo $features['feature_link_"'. $i .'"'] ?>" class="button button-red">Readmore</a>
												</div>
											</div>
										</div>
										<?php
										if(($i == 3) || ($i == 6)):
											?>
										<div class="clearfix"></div>
										<?php
									endif; 
								endif;
							} ?>
						</div>
					</div>
				</div>
			</div>
			<?php 
		endif;

		/* Testimonials Section From Custom Fields */

		$test_enable            = get_field( 'enable_testimonials' ); 
		$test_title             = get_field( 'testimonials_section_title' );
		$test_shortcode         = get_field( 'testimoinals_shortcode' );
		if ( $test_enable ):
			?>
			<div class="section-container bg-lightgreen testimonials-section vz-space clearfix">
				<div class="wrapper">
					<div class="main-title">
						<h2 class="entry-title"><?php echo esc_html($test_title); ?></h2>
					</div>
					<?php  echo do_shortcode( wp_kses_post($test_shortcode) );  ?>
				</div>
			</div>
			<?php
		endif;


		/* Blogs Section */
		$enable_blogs = get_field('enable_blogs_section');
		$blog_section_title = get_field('blog_section_title');

		if($enable_blogs):
			?>
			<!-- BLOGS SECTION -->
			<div id="blogs-section" class="section-container border-lightred vz-space clearfix">
				<div class="wrapper">
					<div class="main-title">
						<h2 class="entry-title"><?php echo $blog_section_title; ?></h2>
					</div>

					<div class="section-row vz-space">
						<?php 

						$args = array(
							'post_type' => 'post',			
							'posts_per_page' => 3,
						);


						$loopPosts = new WP_Query($args);

						if ($loopPosts->have_posts()): 
							while($loopPosts->have_posts()): $loopPosts->the_post(); 

									// wp_setup_postdata();
								$thumbnail_id = get_post_thumbnail_id( get_the_ID() );
								$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true); 
								?>
								<div class="column column-three vz-space blogs_post hz-space">
									<div class="blog_banners">
										<img src="<?php echo get_the_post_thumbnail_url(get_the_ID(),'medium'); ?>"  
										srcset="<?php echo get_the_post_thumbnail_url(get_the_ID(),'medium'); ?> 768w, <?php echo get_the_post_thumbnail_url(get_the_ID(),'thumbnail'); ?> 320w" 
										width="<?php echo get_option( 'medium_size_w' ); ?>" 
										height="200"
										alt="<?php echo $alt; ?>">
									</div>
									<div class="blog_details">
										<div class="entry-block border-animate">
											<h4 class="block-title"><?php echo wp_trim_words( get_the_title(), 4 ); ?></h4>
											<div class="block-description">
												<?php the_excerpt(); ?>
												<a href="<?php echo get_the_permalink(); ?>" class="button button-red">Readmore</a>
											</div>
										</div>
									</div>
								</div>
								<?php 
							endwhile;  
							wp_reset_postdata();

						endif; 

						?>
					</div>
				</div>
			</div>
			<?php 

		endif;
	endif; ?>
</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();