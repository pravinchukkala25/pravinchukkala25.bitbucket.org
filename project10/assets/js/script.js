'use strict';
$(document).ready(function(){
	
  /**
  *  This function is used to get data asynchronously from API URL.
  *  @param  {String} 	url adding API URL.
  *  @return {Function} returns Promise function
  */
  function getPosts(post_url) {
  	return new Promise((resolve, reject) => {
  		$.ajax({
  			type: 'GET',
  			url: post_url,
  			success: (result) => {
  				resolve(result);
  			},
  			error: (error) => {
  				reject(error);
  			}
  		});
  	});
  };

  /**
  *  This function is used create HTML tags with API object properties.
  *  @param  {String} 	imgData passing API Object properties.
  *  @param  {String} 	posts   Number of posts to display.
  */
  let createImg = (imgData, posts) => {
  	for (let i = 0; i < posts; i++) {
  		let createPhoto = `<div class="post" id="${ imgData[i].id }" ><a href="#fixme" title="${ imgData[i].title }"><img src="${ imgData[i].url }" alt="${ imgData[i].title }"></a></div>`;
  		$('.post-entry').append(createPhoto);
  	}
  };

  /**
  *  This function is used create HTML tags with API object properties.
  *  @param  {String} 	postData passing API Object properties.
  *  @param  {String} 	posts   Number of posts to display.
  */
  let createPosts = (postData, posts) => {
  	for (let i = 0; i < posts; i++) {
  		let nthValue = i + 1;
  		let createElement = `<div class="entry-content" id="${ postData[i].id }" ><h3>${ postData[i].title }</h3><p>${ postData[i].body }</p></div>`;
  		$('.post:nth-of-type(' + nthValue + ')').append(createElement);
  	}
  };


	// Creating Promise
	let promise1 = new getPosts('https://jsonplaceholder.typicode.com/photos')
	.then((result) => {
		createImg(result,10);
		return getPosts('https://jsonplaceholder.typicode.com/posts');
	})
	.then((posts) => {
		createPosts(posts, 10);
	});
	

});