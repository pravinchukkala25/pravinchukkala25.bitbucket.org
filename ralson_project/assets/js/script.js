
//------------------------------- Hamburger Dropdown---------------------------
document.getElementById("hamburger").addEventListener("click", hamBurger);

function hamBurger() {
	var hamburger = document.getElementById("hamburger");
	var first = document.getElementById("first-menu");

	if(hamburger.className === "hamburger hidden-lg hidden-md hidden-sm pull-right") {
		hamburger.className = "change-nav hidden-lg hidden-md hidden-sm pull-right";
		first.className = "ul-show";
	}
	else {
		hamburger.className = "hamburger hidden-lg hidden-md hidden-sm pull-right";
		first.className = "ul-hide";
	}
}


//--------------------- Solutions Dropdown------------------------------------
document.getElementById("contains-child").addEventListener("click", secondDropdown);

function secondDropdown() {
	var second = document.getElementById("second-menu");
	if(second.className === "hide-second") {
		second.className = "show-second";
	}
	else {
		second.className = "hide-second";
	}
}


//--------------------- Third Dropdown------------------------------------
document.getElementById("third-menu").addEventListener("click", thirdDropdown);

function thirdDropdown() {
	var third = document.getElementById("third-list");
	var first = document.getElementById("first-menu");
	first.style.height = "85vh";
	if(third.className === "hide-third") {
		third.className = "show-third";
	}
	else {
		third.className = "hide-third";
	}
}


// --------------------Dropdown Toggle------------------------------------------
document.getElementById("settings").addEventListener("click",setting);
document.getElementById("data").addEventListener("click",data);
document.getElementById("utlity").addEventListener("click",utlity);
document.getElementById("manage").addEventListener("click",manage);
document.getElementById("view").addEventListener("click",view);

function setting() {
	var form1 = document.getElementById("form1");
	if( form1.className === "hide-form") { form1.className = "show-form from-group";	}
	else { form1.className = "hide-form";  }
}

function data() {
	var form1 = document.getElementById("form1");
	if( form1.className === "hide-form") { form1.className = "show-form from-group";	}
	else { form1.className = "hide-form";  }
}

function utlity() {
	var form1 = document.getElementById("form1");
	if( form1.className === "hide-form") { form1.className = "show-form from-group";	}
	else { form1.className = "hide-form";  }
}

function manage() {
	var form1 = document.getElementById("form1");
	if( form1.className === "hide-form") { form1.className = "show-form from-group";	}
	else { form1.className = "hide-form";  }
}

function view() {
	var form1 = document.getElementById("form1");
	if( form1.className === "hide-form") { form1.className = "show-form from-group";	}
	else { form1.className = "hide-form";  }
}

//Solutions page Tabs Dropdown Mobile view
var container = document.getElementById("dropdown-tabs");
var child = container.childNodes;
child.className("active").addEventListener("click",tabs);
var childn = child.className("active");
function tabs() {
	if(childn.className === "active"){
		child[1].className = "tabs-dropdown";
		child[2].className = "tabs-dropdown";
		child[3].className = "tabs-dropdown";
		child[4].className = "tabs-dropdown";
	}
	else {
		child[1].className = "active";
		child[2].className = "";
		child[3].className = "";
		child[4].className = "";
	}
}

